//
//     Copyright 2008 Simon Edwards <simon@simonzone.com>

//                 Generated by twine

// This file is part of PyKDE4.

// PyKDE4 is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of
// the License, or (at your option) any later version.

// PyKDE4 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace Akonadi
{
class AgentBase : QObject, QDBusContext
{
%TypeHeaderCode
#include <akonadi/agentbase.h>
%End

public:

    class Observer
    {

    public:
                                Observer ();
        virtual void            itemAdded (const Akonadi::Item& item, const Akonadi::Collection& collection);
        virtual void            itemChanged (const Akonadi::Item& item, const QSet<QByteArray>& partIdentifiers);
        virtual void            itemRemoved (const Akonadi::Item& item);
        virtual void            collectionAdded (const Akonadi::Collection& collection, const Akonadi::Collection& parent);
        virtual void            collectionChanged (const Akonadi::Collection& collection);
        virtual void            collectionRemoved (const Akonadi::Collection& collection);
    };   // Observer


public:

    enum Status
    {
        Idle,
        Running,
        Broken
    };

    virtual int             status () const;
    virtual QString         statusMessage () const;
    virtual int             progress () const;
    virtual QString         progressMessage () const;
    virtual void            configure (WId windowId);
    WId                     winIdForDialogs () const;
    QString                 identifier () const;
    virtual void            cleanup ();
    void                    registerObserver (Akonadi::AgentBase::Observer* observer);

signals:
    void                    status (int status, const QString& message = QString());
    void                    percent (int progress);
    void                    warning (const QString& message);
    void                    error (const QString& message);

protected:
                            AgentBase (const QString& id);
                            ~AgentBase ();
    virtual void            aboutToQuit ();
    Akonadi::ChangeRecorder*  changeRecorder () const;
    void                    changeProcessed ();
    bool                    isOnline () const;
    void                    setOnline (bool state);

//ig     explicit           AgentBase (AgentBasePrivate* d, const QString& id);
    virtual void            doSetOnline (bool online);
};   // AgentBase

};   // Akonadi


