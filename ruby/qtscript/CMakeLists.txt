include_directories( ${CMAKE_SOURCE_DIR}/smoke ${RUBY_INCLUDE_PATH} ${CMAKE_SOURCE_DIR}/ruby/qtruby/src)
INCLUDE_DIRECTORIES (${QT_INCLUDES} ${KDE4_INCLUDE_DIR})

set(qtscript_LIBRARY_SRC qtscript.cpp qtscripthandlers.cpp)

add_library(qtscript MODULE ${qtscript_LIBRARY_SRC})
target_link_libraries(qtscript ${QT_QTCORE_LIBRARY} ${RUBY_LIBRARY} smokeqt smokeqtscript qtruby4shared)
set_target_properties(qtscript PROPERTIES PREFIX "")
install(TARGETS qtscript DESTINATION ${CUSTOM_RUBY_SITE_ARCH_DIR})
install(FILES qtscript.rb DESTINATION ${CUSTOM_RUBY_SITE_LIB_DIR}/qtscript)
