//Auto-generated by kalyptus. DO NOT EDIT.
namespace Nepomuk.Search {
    using Kimono;
    using System;
    using Qyoto;
    using System.Collections.Generic;
    /// <remarks>
    ///  \class QueryServiceClient queryserviceclient.h Nepomuk/Search/QueryServiceClient
    ///  \brief Convenience frontend to the %Nepomuk Query DBus Service
    ///  The QueryServiceClient provides an easy way to access the %Nepomuk Query Service
    ///  without having to deal with any communication details. By default it monitors 
    ///  queries for changes.
    ///  Usage is simple: Create an instance of the client for each search you want to
    ///  track. Once instance may also be reused for subsequent queries if further updates
    ///  of the persistent query are not necessary.
    ///  \author Sebastian Trueg <trueg@kde.org>
    ///           See <see cref="IQueryServiceClientSignals"></see> for signals emitted by QueryServiceClient
    /// </remarks>        <short>    \class QueryServiceClient queryserviceclient.</short>
    [SmokeClass("Nepomuk::Search::QueryServiceClient")]
    public class QueryServiceClient : QObject, IDisposable {
        protected QueryServiceClient(Type dummy) : base((Type) null) {}
        protected new void CreateProxy() {
            interceptor = new SmokeInvocation(typeof(QueryServiceClient), this);
        }
        private static SmokeInvocation staticInterceptor = null;
        static QueryServiceClient() {
            staticInterceptor = new SmokeInvocation(typeof(QueryServiceClient), null);
        }
        /// <remarks>
        ///  Create a new QueryServiceClient instance.
        ///              </remarks>        <short>    Create a new QueryServiceClient instance.</short>
        public QueryServiceClient(QObject parent) : this((Type) null) {
            CreateProxy();
            interceptor.Invoke("QueryServiceClient#", "QueryServiceClient(QObject*)", typeof(void), typeof(QObject), parent);
        }
        public QueryServiceClient() : this((Type) null) {
            CreateProxy();
            interceptor.Invoke("QueryServiceClient", "QueryServiceClient()", typeof(void));
        }
        /// <remarks>
        ///  Start a query using the Nepomuk user query language.
        ///  Results will be reported via newEntries. All results
        ///  have been reported once finishedListing has been emitted.
        ///  \return <pre>true</pre> if the query service was found and the query
        ///  was started. <pre>false</pre> otherwise.
        ///  \sa QueryParser
        ///              </remarks>        <short>    Start a query using the Nepomuk user query language.</short>
        [Q_SLOT("bool query(QString)")]
        public bool Query(string query) {
            return (bool) interceptor.Invoke("query$", "query(const QString&)", typeof(bool), typeof(string), query);
        }
        /// <remarks>
        ///  Start a query.
        ///  Results will be reported via newEntries. All results
        ///  have been reported once finishedListing has been emitted.
        ///  \return <pre>true</pre> if the query service was found and the query
        ///  was started. <pre>false</pre> otherwise.
        ///              </remarks>        <short>    Start a query.</short>
        [Q_SLOT("bool query(Query)")]
        public bool Query(Nepomuk.Search.Query query) {
            return (bool) interceptor.Invoke("query#", "query(const Nepomuk::Search::Query&)", typeof(bool), typeof(Nepomuk.Search.Query), query);
        }
        /// <remarks>
        ///  Start a query using the Nepomuk user query language.
        ///  Results will be reported as with query(string)
        ///  but a local event loop will be started to block the method
        ///  call until all results have been listed.
        ///  The client will be closed after the initial listing. Thus,
        ///  changes to results will not be reported as it is the case
        ///  with the non-blocking methods.
        ///  \return <pre>true</pre> if the query service was found and the query
        ///  was started. <pre>false</pre> otherwise.
        ///  \sa query(string), close()
        ///              </remarks>        <short>    Start a query using the Nepomuk user query language.</short>
        [Q_SLOT("bool blockingQuery(QString)")]
        public bool BlockingQuery(string query) {
            return (bool) interceptor.Invoke("blockingQuery$", "blockingQuery(const QString&)", typeof(bool), typeof(string), query);
        }
        /// <remarks>
        ///  \overload
        ///  \sa query(Query)
        ///              </remarks>        <short>    \overload </short>
        [Q_SLOT("bool blockingQuery(Query)")]
        public bool BlockingQuery(Nepomuk.Search.Query query) {
            return (bool) interceptor.Invoke("blockingQuery#", "blockingQuery(const Nepomuk::Search::Query&)", typeof(bool), typeof(Nepomuk.Search.Query), query);
        }
        /// <remarks>
        ///  Close the client, thus stop to monitor the query
        ///  for changes. Without closing the client it will continue
        ///  signalling changes to the results.
        ///  This will also make any blockingQuery return immediately.
        ///              </remarks>        <short>    Close the client, thus stop to monitor the query  for changes.</short>
        [Q_SLOT("void close()")]
        public void Close() {
            interceptor.Invoke("close", "close()", typeof(void));
        }
        ~QueryServiceClient() {
            interceptor.Invoke("~QueryServiceClient", "~QueryServiceClient()", typeof(void));
        }
        public new void Dispose() {
            interceptor.Invoke("~QueryServiceClient", "~QueryServiceClient()", typeof(void));
        }
        public static new string Tr(string s, string c) {
            return (string) staticInterceptor.Invoke("tr$$", "tr(const char*, const char*)", typeof(string), typeof(string), s, typeof(string), c);
        }
        public static new string Tr(string s) {
            return (string) staticInterceptor.Invoke("tr$", "tr(const char*)", typeof(string), typeof(string), s);
        }
        /// <remarks>
        ///  Check if the service is running.
        ///  \return <pre>true</pre> if the Nepomuk query service is running and could
        ///  be contacted via DBus, <pre>false</pre> otherwise
        ///              </remarks>        <short>    Check if the service is running.</short>
        public static bool ServiceAvailable() {
            return (bool) staticInterceptor.Invoke("serviceAvailable", "serviceAvailable()", typeof(bool));
        }
        protected new IQueryServiceClientSignals Emit {
            get { return (IQueryServiceClientSignals) Q_EMIT; }
        }
    }

    public interface IQueryServiceClientSignals : IQObjectSignals {
        /// <remarks>
        ///  Emitted for new search results. This signal is emitted both
        ///  for the initial listing and for changes to the search.
        ///              </remarks>        <short>    Emitted for new search results.</short>
        [Q_SIGNAL("void newEntries(QList<Nepomuk::Search::Result>)")]
        void NewEntries(List<Nepomuk.Search.Result> entries);
        /// <remarks>
        ///  Emitted if the search results changed when monitoring a query.
        ///  \param entries A list of resource URIs identifying the resources
        ///  that dropped out of the query results.
        ///              </remarks>        <short>    Emitted if the search results changed when monitoring a query.</short>
        [Q_SIGNAL("void entriesRemoved(QList<QUrl>)")]
        void EntriesRemoved(List<QUrl> entries);
        /// <remarks>
        ///  Emitted when the initial listing has been finished, ie. if all 
        ///  results have been reported via newEntries. If no further updates
        ///  are necessary the client should be closed now.
        ///              </remarks>        <short>    Emitted when the initial listing has been finished, ie.</short>
        [Q_SIGNAL("void finishedListing()")]
        void FinishedListing();
    }
}
