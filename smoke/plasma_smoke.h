#ifndef PLASMA_SMOKE_H
#define PLASMA_SMOKE_H

#include <smoke.h>

extern SMOKE_EXPORT Smoke* plasma_Smoke;
extern SMOKE_EXPORT void init_plasma_Smoke();

#ifndef QGLOBALSPACE_CLASS
#define QGLOBALSPACE_CLASS
class QGlobalSpace { };
#endif

#endif
